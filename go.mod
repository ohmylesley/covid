module gitlab.com/ohmylesley/covid

go 1.15

require (
	github.com/aws/aws-lambda-go v1.19.1
	github.com/gorilla/mux v1.8.0
)
