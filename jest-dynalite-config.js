module.exports = {
  tables: [
    {
      TableName: "data1",
      KeySchema: [
        { AttributeName: "state", KeyType: "HASH" },
        { AttributeName: "datestamp", KeyType: "RANGE" },
      ],
      AttributeDefinitions: [
        { AttributeName: "state", AttributeType: "S" },
        { AttributeName: "datestamp", AttributeType: "N" },
      ],
      ProvisionedThroughput: {
        ReadCapacityUnits: 1,
        WriteCapacityUnits: 1,
      },
    },
    {
      TableName: "cssefiles",
      KeySchema: [
        { AttributeName: "filename", KeyType: "HASH" },
        { AttributeName: "lastparse", KeyType: "RANGE" },
      ],
      AttributeDefinitions: [
        { AttributeName: "filename", AttributeType: "S" },
        { AttributeName: "lastparse", AttributeType: "N" },
      ],
      ProvisionedThroughput: {
        ReadCapacityUnits: 1,
        WriteCapacityUnits: 1,
      },
    },
  ],
  basePort: 8000,
};
