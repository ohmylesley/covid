const { task, desc } = require("jake");
const Bundler = require("parcel-bundler");
const path = require("path");
const del = require("del");

desc("Bundle handler");
task("bundle", async function (f1) {
  const entryFiles = path.join(__dirname, `./src/handler/${f1}.js`);
  const options = {
    scopeHoist: true,
    outDir: "./build",
    watch: false,
    target: "node",
    sourceMaps: false,
    detailedReport: true,
  };
  const b = new Bundler(entryFiles, options);
  return b.bundle();
});

desc("Clean build folder");
task("clean", async () => {
  return del(["build/**"]);
});
