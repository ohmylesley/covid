const { DateTime } = require("luxon");
const R = require("ramda");

const la = "America/Los_Angeles";

const invokeM = (o) => o.toMillis();

// str 04-13-2020
const token = "LL-dd-yyyy z";
const extractJhDate = (str) => {
  return DateTime.fromFormat(`${str} ${la}`, token);
};

const diffDays = (a, b) => a.diff(b, "days").days;

const isBetween = ({ l, r, x }) => diffDays(x, l) >= 0 && diffDays(r, x) >= 0;
const filterDate = R.pipe(R.map(extractJhDate), isBetween);

module.exports = {
  extractJhDate,
  diffDays,
  isBetween,
  getJHMill: R.pipe(R.prop("str"), extractJhDate, invokeM),
  filterDate,
};
