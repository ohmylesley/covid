const { Record } = require("immutable");

// Default param for step function
const makeParam = Record({
  url: "https://api.github.com/graphql",
  ghtoken: "",
  repo: "COVID-19",
  folderpath: "master:csse_covid_19_data/csse_covid_19_daily_reports_us",
  table: "",
  bufferSize: 10,
  owner: "CSSEGISandData",
  listSize: 4,
  filesTable: "",
  noParseIdVal: 100,
  parseIdVal: 200,
  isLastRun: true
});

module.exports = {
  makeParam,
};
