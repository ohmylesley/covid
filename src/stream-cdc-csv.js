const csv = require("csv-parser");
const { merge, fromEvent, from } = require("rxjs");
const {
  map,
  mapTo,
  takeUntil,
  skip,
  take,
  filter,
  share,
  bufferCount,
  mergeMap,
  count,
} = require("rxjs/operators");
const { getTimestamp } = require("./date");
const { fromJS } = require("immutable");

const streamCsv = ({ AWS, bucket, csvKey, parseOption }) => {
  const s3 = new AWS.S3();
  const requestStream = s3
    .getObject({
      Bucket: bucket,
      Key: csvKey,
    })
    .createReadStream()
    .pipe(csv(parseOption || {}));

  const data$ = fromEvent(requestStream, "data").pipe(
    map((x) => ({
      code: 200,
      data: x,
    }))
  );
  const end$ = fromEvent(requestStream, "close").pipe(
    mapTo({
      code: 400,
    })
  );
  const close$ = fromEvent(requestStream, "end");
  return merge(data$, end$).pipe(takeUntil(close$));
};

const streamDb = ({
  upStream,
  dbClient,
  table,
  bufferSize,
  startIndex,
  pageSize,
}) => {
  const input$ = share()(upStream);
  const filteredData$ = input$.pipe(
    filter((x) => x.code === 200),
    map((x) => fromJS(x)),
    skip(startIndex - 1),
    take(pageSize)
  );
  const countFiltered$ = filteredData$.pipe(
    count(),
    map((n) => ({
      code: 201,
      data: n,
    }))
  );
  const data$ = filteredData$.pipe(
    map((x) =>
      x.setIn(
        ["data", "datestamp"],
        getTimestamp({
          str: x.getIn(["data", "submission_date"]),
        })
      )
    ),
    map((x) => ({
      PutRequest: {
        Item: x.get("data").toJS(),
      },
    })),
    bufferCount(bufferSize),
    mergeMap((arr) =>
      from(
        dbClient
          .batchWrite({
            RequestItems: {
              [table]: arr,
            },
          })
          .promise()
      )
    ),
    map((res) => ({
      code: 200,
      data: res,
    }))
  );
  return merge(data$, countFiltered$);
};

module.exports = {
  streamCsv,
  streamDb,
};
