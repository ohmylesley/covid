const batchWrite = ({ bacon, table, dbClient, bufferSize }) => (ob) =>
  ob
    .map((x) => ({
      PutRequest: {
        Item: x,
      },
    }))
    .bufferWithCount(bufferSize)
    .flatMap((arr) =>
      bacon.fromPromise(
        dbClient
          .batchWrite({
            RequestItems: {
              [table]: arr,
            },
          })
          .promise()
      )
    );

module.exports = {
  batchWrite,
};
