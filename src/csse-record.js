const { Record } = require("immutable");

const makeCsse = Record({
  state: "us",
  datestamp: 1,
  filename:"",
  Last_Update: "",
  Confirmed: "",
  Deaths: "",
  Recovered: "",
  Active: "",
  Incident_Rate: "",
  People_Tested: "",
  People_Hospitalized: "",
  Mortality_Rate: "",
  UID: "",
  Testing_Rate: "",
  Hospitalization_Rate: "",
});

module.exports = {
  makeCsse,
};
