const Readable = require("stream").Readable;

const textToReadable = (text) => {
  const s = new Readable();
  s.push(text);
  s.push(null);
  return s;
};

const streamToOb = ({ fromEvent }) => (s) => {
  const data = fromEvent(s, "data");
  const end = fromEvent(s, "end");
  return data.takeUntil(end);
};

module.exports = {
  textToReadable,
  streamToOb,
};
