const AWS = require("aws-sdk");
const { batchWrite } = require("../db");
const { getAndNorm } = require("../jh/getfiles");
const bacon = require("baconjs");
const axios = require("axios");

//Get files under JH github and save filenames to db
exports.getFiles = async (event) => {
  const {
    table,
    bufferSize,
    region,
    ghtoken,
    url,
    owner,
    repo,
    folderpath,
    noParseIdVal,
    leftDate,
    rightDate,
  } = event;
  const bufsz = parseInt(bufferSize) || 10;
  const dbregion = region || "us-east-1";
  const dbClient = new AWS.DynamoDB.DocumentClient({
    region: dbregion,
  });
  const save = batchWrite({
    bacon,
    table,
    dbClient,
    bufferSize: bufsz,
  });
  const reqStraem = getAndNorm({
    bacon,
    axios,
    url,
    token: ghtoken,
    owner,
    repo,
    folderpath,
    leftDate,
    rightDate,
    unparsedId: noParseIdVal,
  });
  return save(reqStraem)
    .map({
      code: 200,
    })
    .toPromise();
};
