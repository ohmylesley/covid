const { streamCsv, streamDb } = require("../stream-cdc-csv");
const AWS = require("aws-sdk");
const { filter } = require("rxjs/operators");

exports.csvtodb = function (event, ctx, callback) {
  const {
    region,
    startIndex,
    pageSize,
    csvBucket,
    csvKey,
    table,
    bufferSize,
  } = event;
  const dbregion = region || "us-east-1";
  const dbClient = new AWS.DynamoDB.DocumentClient({
    region: dbregion,
  });

  const start1 = startIndex >= 1 ? startIndex : 1
  const pgsize = pageSize >= 1 ? pageSize : 20
  const bufsize = bufferSize >= 1 ? bufferSize : 10

  const csv$ = streamCsv({
    AWS,
    bucket: csvBucket,
    csvKey,
  });
  streamDb({
    upStream: csv$,
    dbClient,
    table,
    bufferSize: bufsize,
    startIndex: start1,
    pageSize: pgsize,
  })
    .pipe(filter((x) => x.code == 201))
    .subscribe({
      next: (x) => {
        const eof = x.data < pageSize;
        const code = eof ? 401 : 200;
        callback(null, {
          code,
          startIndex: startIndex + pageSize,
          datacount: x.data,
          region,
          pageSize: pgsize,
          csvBucket,
          csvKey,
          table,
          bufferSize: bufsize,
        });
      },
      error: (e) => {
        callback(e);
      },
      complete: () => {
        callback(null, {
          code: 400,
        });
      },
    });
};
