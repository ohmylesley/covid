const AWS = require("aws-sdk");
const { batchWrite } = require("../db");
const { getAndNorm } = require("../jh/getblob");
const bacon = require("baconjs");
const csv = require("csv-parser");
const axios = require("axios");

//parse single csv from JH github repo into db
const singlefile = async (event) => {
  const {
    table,
    bufferSize,
    region,
    ghtoken,
    url,
    owner,
    repo,
    folderpath,
    fileKey,
  } = event;
  const bufsz = parseInt(bufferSize) || 10;
  const dbregion = region || "us-east-1";
  const dbClient = new AWS.DynamoDB.DocumentClient({
    region: dbregion,
  });
  const save = batchWrite({
    table,
    dbClient,
    bufferSize: bufsz,
    bacon,
  });
  const req = getAndNorm({
    bacon,
    axios,
    csv,
    filename: fileKey,
    filepath: `${folderpath}/${fileKey}`,
    token: ghtoken,
    url,
    owner,
    repo,
  });
  return save(req).toPromise();
};

//parse one from a list of filenames into db; step function
const filelist = async (event) => {
  const list1 = event.fileList;
  const index = event.currentIndex;
  if (index >= list1.length || index < 0) {
    return {
      ...event,
      complete: true,
    };
  }

  const currentFile = list1[index];
  //console.log("current file", currentFile);
  const ev = {
    ...event,
    fileKey: currentFile,
  };
  return singlefile(ev).then(() => {
    const res = {
      ...event,
      currentIndex: index + 1,
      complete: index >= list1.length - 1,
    };
    return res;
  });
};

module.exports = {
  singlefile,
  filelist,
};
