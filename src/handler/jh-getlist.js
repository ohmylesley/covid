const AWS = require("aws-sdk");

//query a list of filenames from db for step function to parse
const getList = async (event) => {
  const size = event.listSize || 10;
  const filesTable = event.filesTable;
  const hkey1 = event.noParseIdVal || 2;
  const dbregion = event.region || "us-east-1";
  const dbClient = new AWS.DynamoDB.DocumentClient({
    region: dbregion,
  });
  const param = {
    TableName: filesTable,
    KeyConditionExpression: "isparsed = :hkey",
    ExpressionAttributeValues: {
      ":hkey": hkey1,
    },
    Limit: size,
  };
  return dbClient
    .query(param)
    .promise()
    .then((x) => x.Items)
    .then((arr) => arr.map((x) => x.filename))
    .then((arr) => ({
      ...event,
      fileList: arr,
      currentIndex: 0,
      isLastRun: arr.length < size,
      needParse: arr != null && arr.length > 0,
    }));
};

module.exports = {
  getList,
};
