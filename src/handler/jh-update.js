const AWS = require("aws-sdk");
const { makeParam } = require("../param-record");

// Update file parsed info in db after parse. Step function.
const setFileParsed = async (event) => {
  const filesTable = event.filesTable;
  const fileList = event.fileList;

  if (fileList == null || fileList.length < 1) {
    return {
      complete: true,
      isLastRun: true,
    };
  }

  const hashKey1 = event.noParseIdVal;
  const hashKey2 = event.parseIdVal;
  const delObjects = fileList.map((filename) => ({
    DeleteRequest: {
      Key: {
        isparsed: hashKey1,
        filename,
      },
    },
  }));
  const putObjects = fileList.map((n) => ({
    PutRequest: {
      Item: {
        isparsed: hashKey2,
        filename: n,
        timestamp: new Date().getTime(),
      },
    },
  }));
  const params = {
    RequestItems: {
      [filesTable]: delObjects.concat(putObjects),
    },
  };
  const client = new AWS.DynamoDB.DocumentClient({
    region: "us-east-1",
  });
  return client
    .batchWrite(params)
    .promise()
    .then(() => makeParam(event).toJS());
};

module.exports = {
  setFileParsed,
};
