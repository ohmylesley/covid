const { makeHeader } = require("../gql");
const { filterDate } = require("../date");
const { propd2 } = require("../funcs");

const gettree = `
query Gettree($owner: String!, $repo: String!, $folderpath: String!){
  repository(name:$repo,owner:$owner){
    object(expression:$folderpath){
      ... on Tree {
        commitUrl
        entries{
          name
        }
      }
    }
  }
}
`;

const getFiles = ({ bacon, axios, url, token, owner, repo, folderpath }) =>
  bacon
    .fromPromise(
      axios({
        method: "post",
        url,
        data: {
          query: gettree,
          variables: {
            owner,
            repo,
            folderpath,
          },
        },
        headers: makeHeader(token),
      })
    )
    .map(propd2);

const csvReg = /^\d{2}-\d{2}-\d{4}\.csv$/;
const filterCsv = ({ bacon }) => (ob) =>
  ob
    .map((x) => x.repository.object.entries)
    .flatMap(bacon.fromArray)
    .map((x) => x.name)
    .filter((x) => x && csvReg.test(x));

const filterFileDate = (l, r) => (str) => {
  const dateStr = str.substring(0, 10);
  return filterDate({
    l,
    r,
    x: dateStr,
  });
};

const toItem = ({ unparsedId }) => (name) => ({
  filename: name,
  isparsed: unparsedId,
});

const getAndNorm = (param) => {
  const { bacon, leftDate, rightDate, unparsedId } = param;
  return filterCsv({ bacon })(getFiles(param))
    .filter(filterFileDate(leftDate, rightDate))
    .map(toItem({ unparsedId }));
};

module.exports = {
  getFiles,
  filterCsv,
  filterFileDate,
  toItem,
  getAndNorm,
};
