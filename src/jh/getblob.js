const { textToReadable, streamToOb } = require("../observable");
const { makeCsse } = require("../csse-record");
const { getJHMill } = require("../date");
const { makeHeader } = require("../gql.js");
const { propd2 } = require("../funcs");

const blobQuery = `
query Gettext($owner: String!, $repo: String!, $filepath: String!){
  repository(name:$repo,owner:$owner){
    object(expression:$filepath){
      ... on Blob {
        text
      }
    }
  }
}
`;
const getBlob = ({ bacon, axios, url, token, owner, repo, filepath }) =>
  bacon
    .fromPromise(
      axios({
        method: "post",
        url,
        data: {
          query: blobQuery,
          variables: {
            owner,
            repo,
            filepath,
          },
        },
        headers: makeHeader(token),
      })
    )
    .map(propd2);

const textToCsv = ({ csv, of, fromEvent }) => (ob) =>
  of(ob)
    .map((x) => x.repository.object.text)
    .map(textToReadable)
    .map((s) => s.pipe(csv()))
    .flatMap(streamToOb({ fromEvent }));

const normalize = ({ filename, of }) => (o) => {
  return of(o)
    .map(makeCsse)
    .filter((r) => r.get("state") != null && r.get("state").length > 0)
    .map((r) => r.set("state", o.Province_State))
    .map((r) =>
      r.set("datestamp", getJHMill({ str: filename.substring(0, 10) }))
    )
    .map((r) => r.set("filename", filename))
    .map((r) => r.toJS());
};

const getAndNorm = (param) => {
  const { bacon, csv, filename } = param;
  return getBlob(param)
    .flatMap(textToCsv({ csv, of: bacon.once, fromEvent: bacon.fromEvent }))
    .flatMap(normalize({ filename, of: bacon.once }));
};

module.exports = {
  getBlob,
  textToCsv,
  normalize,
  getAndNorm
};
