const makeHeader = (token) => ({
  "Content-Type": "application/json",
  Accept: "application/json",
  Authorization: `bearer ${token}`,
});

module.exports = {
  makeHeader,
};
