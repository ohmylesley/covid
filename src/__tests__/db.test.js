const bacon = require("baconjs");
const { batchWrite } = require("../db");
const AWS = require("aws-sdk");

describe("db", () => {
  it("batchwrite", () => {
    const dbClient = new AWS.DynamoDB.DocumentClient({
      endpoint: process.env.MOCK_DYNAMODB_ENDPOINT,
      sslEnabled: false,
      region: "local",
    });
    const table = "data1";
    const arr1 = [
      {
        state: "AAA",
        datestamp: 12,
      },
      {
        state: "MA",
        datestamp: 22,
      },
    ];
    const arr = bacon.fromArray(arr1);
    const scan = bacon.fromNodeCallback(dbClient.scan.bind(dbClient), {
      TableName: table,
    });
    return batchWrite({
      bacon,
      table,
      dbClient,
      bufferSize: 2,
    })(arr)
      .flatMapLatest(scan)
      .map((x) => x.Items)
      .doAction((x) => {
        expect(x).toEqual(arr1);
      })
      .toPromise();
  });
});
