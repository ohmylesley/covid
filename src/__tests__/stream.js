const { streamCsv, streamDb } = require("../streamcsv");
const path = require("path");
const fs = require("fs");
const AWS = require("aws-sdk");

describe("parse", () => {
  let dbClient;
  let s3mock;
  beforeEach(() => {
    //console.log("local dynamo:", process.env.MOCK_DYNAMODB_ENDPOINT);
    dbClient = new AWS.DynamoDB.DocumentClient({
      endpoint: process.env.MOCK_DYNAMODB_ENDPOINT,
      sslEnabled: false,
      region: "local",
    });
    s3mock = jest.fn(() => {
      return {
        getObject: () => ({
          createReadStream: () =>
            fs.createReadStream(path.resolve(__dirname, "./sampledata1.csv")),
        }),
      };
    });
  });
  it("streamcsv", (done) => {
    const dataCb = jest.fn();
    const endCB = jest.fn();
    const completeCb = jest.fn();
    const param = {
      bucket: "mock1",
      csvKey: "mock2",
      AWS: { S3: s3mock },
    };

    streamCsv(param).subscribe({
      next: (x) => {
        if (x.code == 200) {
          dataCb();
          expect(x.data).toHaveProperty("state");
          expect(x.data).toHaveProperty("submission_date");
        } else {
          endCB();
          expect(x.code).toEqual(400);
        }
      },
      error: (e) => {
        console.log("error:", e);
        done();
      },
      complete: () => {
        completeCb();
        expect(dataCb.mock.calls.length).toBe(40);
        expect(completeCb.mock.calls.length).toBe(1);
        expect(endCB.mock.calls.length).toBe(1);
        done();
      },
    });
  });
  it("streamdb", (done) => {
    const datacb = jest.fn();
    const endcb = jest.fn();
    const completecb = jest.fn();
    const countcb = jest.fn();
    const up$ = streamCsv({
      AWS: { S3: s3mock },
    });
    streamDb({
      upStream: up$,
      startIndex: 4,
      pageSize: 20,
      bufferSize: 3,
      dbClient,
      table: "data1",
    }).subscribe({
      next: (x) => {
        if (x.code == 400) {
          endcb();
        } else if (x.code == 200) {
          datacb();
        } else {
          countcb();
        }
      },
      error: (e) => {
        console.log("error:", e);
      },
      complete: () => {
        completecb();
        expect(datacb.mock.calls.length).toEqual(7);
        expect(endcb.mock.calls.length).toEqual(0);
        expect(countcb.mock.calls.length).toEqual(1);
        expect(completecb.mock.calls.length).toEqual(1);
        done();
      },
    });
  });
  it("streamdb2", (done) => {
    const datacb = jest.fn();
    const endcb = jest.fn();
    const completecb = jest.fn();
    const countcb = jest.fn();
    const up$ = streamCsv({
      AWS: { S3: s3mock },
    });
    streamDb({
      upStream: up$,
      startIndex: 35,
      pageSize: 20,
      bufferSize: 3,
      dbClient,
      table: "data1",
    }).subscribe({
      next: (x) => {
        if (x.code == 400) {
          endcb();
        } else if (x.code == 200) {
          datacb();
        } else {
          countcb();
          expect(x.data).toEqual(6);
        }
      },
      error: (e) => {
        console.log("error:", e);
      },
      complete: () => {
        completecb();
        expect(datacb.mock.calls.length).toEqual(2);
        expect(endcb.mock.calls.length).toEqual(0);
        expect(countcb.mock.calls.length).toEqual(1);
        expect(completecb.mock.calls.length).toEqual(1);
        done();
      },
    });
  });

  it("streamdb3", (done) => {
    const datacb = jest.fn();
    const endcb = jest.fn();
    const completecb = jest.fn();
    const countcb = jest.fn();
    const up$ = streamCsv({
      AWS: { S3: s3mock },
    });
    streamDb({
      upStream: up$,
      startIndex: 50,
      pageSize: 20,
      bufferSize: 3,
      dbClient,
      table: "data1",
    }).subscribe({
      next: (x) => {
        if (x.code == 400) {
          endcb();
        } else if (x.code == 200) {
          datacb();
        } else {
          countcb();
          expect(x.data).toEqual(0);
        }
      },
      error: (e) => {
        console.log("error:", e);
      },
      complete: () => {
        completecb();
        expect(datacb.mock.calls.length).toEqual(0);
        expect(endcb.mock.calls.length).toEqual(0);
        expect(countcb.mock.calls.length).toEqual(1);
        expect(completecb.mock.calls.length).toEqual(1);
        done();
      },
    });
  });
  it("streamdb4", (done) => {
    const datacb = jest.fn();
    const endcb = jest.fn();
    const completecb = jest.fn();
    const countcb = jest.fn();
    const up$ = streamCsv({
      AWS: { S3: s3mock },
    });
    streamDb({
      upStream: up$,
      startIndex: 1,
      pageSize: 200,
      bufferSize: 20,
      dbClient,
      table: "data1",
    }).subscribe({
      next: (x) => {
        if (x.code == 400) {
          endcb();
        } else if (x.code == 200) {
          datacb();
        } else {
          countcb();
          expect(x.data).toEqual(40);
        }
      },
      error: (e) => {
        console.log("error:", e);
      },
      complete: () => {
        completecb();
        expect(datacb.mock.calls.length).toEqual(2);
        expect(endcb.mock.calls.length).toEqual(0);
        expect(countcb.mock.calls.length).toEqual(1);
        expect(completecb.mock.calls.length).toEqual(1);
        done();
      },
    });
  });
});
