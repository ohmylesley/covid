const { csvtodb } = require("../handler/cdc-s3");
const AWS = require("aws-sdk");
const { streamCsv } = require("../stream-jh");

describe("cdcdata", () => {
  const event1 = {
    startIndex: 2,
    pageSize: 21,
    csvBucket: "serverless-artifact-100",
    csvKey: "sampledata1.csv",
    table: "covid-data",
    bufferSize: 10,
  };
  it("integration1", (done) => {
    const cb = (e, data) => {
      if (e) {
        console.log("error", e);
      } else {
        expect(data).toEqual({
          code: 200,
          startIndex: 23,
          region: "us-east-1",
          pageSize: 21,
          csvBucket: event1.csvBucket,
          table: event1.table,
          bufferSize: 10,
        });
      }
      done();
    };
    csvtodb(event1, null, cb);
  });

  it("int-csv", (done) => {
    streamCsv({
      AWS,
      bucket: event1.csvBucket,
      csvKey: event1.csvKey,
    }).subscribe({
      next: (x) => {
        console.log(x);
      },
      error: (e) => {
        console.log(e);
        done();
      },
      complete: () => {
        console.log("complete");
        done();
      },
    });
  });
});

