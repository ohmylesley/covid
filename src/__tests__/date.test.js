const {
  extractJhDate,
  diffDays,
  isBetween,
  getJHMill,
  filterDate,
} = require("../date");

describe("date", () => {
  it("jhdate", () => {
    const s1 = "06-22-2020";
    const rfc2822Str = "Mon, 22 Jun 2020 00:00:00 -0700";
    const lx = extractJhDate(s1);
    const m1 = new Date(rfc2822Str).getTime();
    expect(lx.year).toBe(2020);
    expect(lx.month).toBe(6);
    expect(lx.day).toBe(22);
    expect(lx.toMillis()).toEqual(m1);

    const p = {
      str: s1,
    };
    expect(m1).toEqual(getJHMill(p));
  });
  it("diffdays", () => {
    const t1 = "10-28-2020";
    const t2 = "11-04-2020";
    const lx1 = extractJhDate(t1);
    const lx2 = extractJhDate(t2);
    const df = diffDays(lx1, lx2);
    expect(df).toBe(-7);
  });
  it("between", () => {
    const t1 = "10-28-2020";
    const t2 = "11-04-2020";
    const lx1 = extractJhDate(t1);
    const lx2 = extractJhDate(t2);
    const lx3 = extractJhDate("10-30-2020");
    const lx4 = extractJhDate("11-05-2020");
    const lx5 = extractJhDate("01-30-2020");
    const f = (d) =>
      isBetween({
        l: lx1,
        r: lx2,
        x: d,
      });
    expect(f(lx2)).toBe(true);
    expect(f(lx3)).toBe(true);
    expect(f(lx4)).toBe(false);
    expect(f(lx5)).toBe(false);
  });
  it("filterdate", () => {
    const p = (x) => ({
      l: "05-01-2020",
      r: "06-20-2020",
      x,
    });
    const p1 = p("05-02-2020");
    const p2 = p("07-02-2020");
    expect(filterDate(p1)).toBe(true);
    expect(filterDate(p2)).toBe(false);
  });
});
