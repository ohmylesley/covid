const AWS = require("aws-sdk");
const { getFiles } = require("../handler/jh-filelist");
const { getList } = require("../handler/jh-getlist");
const { singlefile, filelist } = require("../handler/jh-parse");

describe("johnhopkins", () => {
  const url = "https://api.github.com/graphql";
  const token1 = process.env.GHTOKEN1;
  const repo = "COVID-19";
  const owner = "CSSEGISandData";
  const folderpath = "master:csse_covid_19_data/csse_covid_19_daily_reports_us";
  beforeAll(() => {
    AWS.config.credentials = new AWS.SharedIniFileCredentials({
      profile: "default",
    });
  });
  it("getfiles", () => {
    const table = "test-files1";
    return getFiles({
      url,
      table,
      ghtoken: token1,
      owner,
      folderpath,
      repo,
      bufferSize: 10,
      noParseIdVal: 13,
      leftDate: "08-02-2020",
      rightDate: "08-10-2020",
    }).then((x) => {
      expect(x).toEqual({
        code: 200,
      });
    });
  });
  it("parsejh1", () => {
    const table = "covid-dev3-jh";
    const file1 = "04-14-2020.csv";
    return singlefile({
      url,
      table,
      ghtoken: token1,
      owner,
      folderpath,
      repo,
      fileKey: file1,
      bufferSize: 15,
    });
  });
  it("filelist", () => {
    const table = "covid-dev3-jh";
    const list1 = [
      "04-14-2020.csv",
      "04-15-2020.csv",
      "04-16-2020.csv",
      "04-17-2020.csv",
    ];
    return filelist({
      url,
      table,
      ghtoken: token1,
      owner,
      folderpath,
      repo,
      bufferSize: 15,
      fileList: list1,
      currentIndex: 2,
    }).then((x) => {
      expect(x.currentIndex).toEqual(3);
    });
  });
  it("getlist", () => {
    const filesTable = "test-files1";
    return getList({
      filesTable,
      listSize: 5,
      url: "someurl",
      bufferSize: 12,
      noParseIdVal: 100,
    }).then((x) => {
      console.log(x);
    });
  });
  /*
  it("setparsed", () => {
    const ev = {
      filesTable: "test-updatefile",
      fileList: ["test1", "test2", "test3", "test4"],
      noParseIdVal: 2,
      parseIdVal: 101,
    };
    return setFileParsed(ev).then((x) => {
      console.log(x);
    });
  });*/
});
