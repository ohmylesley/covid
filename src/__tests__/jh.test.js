const {
  getFiles,
  filterCsv,
  filterFileDate,
  getAndNorm,
} = require("../jh/getfiles");
const fs = require("fs").promises;
const path = require("path");
const bacon = require("baconjs");
const axios = require("axios");
const { textToCsv, normalize, getAndNorm: g2 } = require("../jh/getblob");
const csv = require("csv-parser");

describe("parse-jh", () => {
  const csvReg = /^\d{2}-\d{2}-\d{4}\.csv$/;
  const url = "https://api.github.com/graphql";
  const token = process.env.GHTOKEN1;
  const repo = "COVID-19";
  const owner = "CSSEGISandData";
  const folder = "master:csse_covid_19_data/csse_covid_19_daily_reports_us";
  const param1 = {
    url,
    token,
    repo,
    owner,
    folderpath: folder,
  };
  const csseFile = path.resolve(__dirname, "./csse-example.csv");
  const fileStream = bacon.fromPromise(
    fs.readFile(csseFile, {
      encoding: "utf8",
    })
  );
  const wrapOb = (str) => ({
    repository: {
      object: {
        text: str,
      },
    },
  });

  it("gettree", () => {
    const f = filterCsv({
      bacon,
    });
    const req = getFiles({
      bacon,
      axios,
      ...param1,
    });
    return f(req)
      .take(5)
      .doAction((x) => {
        expect(x).toMatch(csvReg);
      })
      .toPromise();
  });
  it("normalize", () => {
    const datacb = jest.fn();
    const p = {
      ...param1,
      bacon,
      unparsedId: 22,
      leftDate: "05-01-2020",
      rightDate: "05-06-2020",
      axios,
    };
    const s = getAndNorm(p).doAction((x) => {
      datacb();
      expect(x).toEqual({
        isparsed: 22,
        filename: expect.stringMatching(csvReg),
      });
    });
    s.onEnd(() => {
      expect(datacb.mock.calls.length).toEqual(6);
    });
    return s.toPromise();
  });
  it("getblob", () => {
    const file1 = "05-01-2020.csv";
    const param = {
      ...param1,
      filename: file1,
      bacon,
      csv,
      axios,
      filepath: folder + "/" + file1,
    };
    return g2(param)
      .doAction((x) => {
        expect(x.datestamp > 10000000).toEqual(true);
        expect(x.state).toBeTruthy();
        expect(x).toHaveProperty("filename", file1);
      })
      .toPromise();
  });
  it("normjh", () => {
    const datacb = jest.fn();
    const name1 = "05-12-2020.csv";
    const s = fileStream
      .map(wrapOb)
      .flatMap(textToCsv({ csv, of: bacon.once, fromEvent: bacon.fromEvent }))
      .flatMap(normalize({ filename: name1, of: bacon.once }))
      .doAction((x) => {
        datacb();
        expect(x.datestamp > 10000000).toEqual(true);
        expect(x.state).toBeTruthy();
        expect(x).toHaveProperty("filename", name1);
      });
    s.onEnd(() => {
      expect(datacb.mock.calls.length).toEqual(19);
    });
    return s.toPromise();
  });
  it("filterDate", () => {
    const l = "05-29-2020";
    const r = "07-29-2020";

    const apr = ["04-01-2020", "04-01-2020.csv"];
    const may = [l, "06-01-2020.txt"];
    const s1 = bacon.fromArray(apr);
    const s2 = bacon.fromArray(may);
    return s1
      .merge(s2)
      .filter(filterFileDate(l, r))
      .reduce([], (acc, x) => acc.concat(x))
      .doAction((x) => {
        expect(x).toEqual(may);
      })
      .toPromise();
  });
});
