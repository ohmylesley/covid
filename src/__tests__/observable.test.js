const { textToReadable, streamToOb } = require("../observable");
const csv = require("csv-parser");
const fs = require("fs").promises;
const path = require("path");
const bacon = require("baconjs");
const stream = require("stream");

describe("observable", () => {
  let fileStream;
  beforeEach(() => {
    const csseFile = path.resolve(__dirname, "./csse-example.csv");
    fileStream = bacon.fromPromise(
      fs.readFile(csseFile, {
        encoding: "utf8",
      })
    );
  });
  it("textstream", () => {
    const datacb = jest.fn();
    const s = fileStream
      .map(textToReadable)
      .doAction((s) => {
        expect(s).toBeInstanceOf(stream.Readable);
      })
      .map((s) => s.pipe(csv()))
      .flatMap(
        streamToOb({
          fromEvent: bacon.fromEvent,
        })
      )
      .doAction((x) => {
        datacb();
        expect(x).toHaveProperty("Province_State");
        expect(x).toHaveProperty("Confirmed");
      });
    s.onEnd(() => {
      expect(datacb.mock.calls.length).toEqual(19);
    });
    return s.toPromise();
  });
});
