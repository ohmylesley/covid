package main

import (
	"context"
	"encoding/json"
	"github.com/aws/aws-lambda-go/lambda"
	"gitlab.com/ohmylesley/covid/src/server/service"
	"log"
)

type Any interface{}

type Event struct {
	HttpMethod string `json:"httpMethod"`
	Body       string `json:"body"`
}

type Response struct {
	Status  int    `json:"statusCode"`
	Body    []byte `json:"body"`
	Headers Any    `json:"headers"`
}

type Result struct {
	Code string           `json:"code"`
	Data service.SirSlice `json:"data"`
}

type Headers struct {
	Origin string `json:"Access-Control-Allow-Origin"`
}

func HandleRequest(ctx context.Context, e Event) (Response, error) {
	log.Println(e)
	var inputInitData service.InitialData
	e1 := json.Unmarshal([]byte(e.Body), &inputInitData)
	if e1 != nil {
		log.Println(e1)
	}
	sirSlice := service.GetSir(inputInitData)
	bodyJson, err := json.Marshal(Result{"sir001", sirSlice})
	if err != nil {
		log.Println(err)
	}
	return Response{
		Status:  200,
		Body:    bodyJson,
		Headers: Headers{"*"},
	}, nil
}

func main() {
	lambda.Start(HandleRequest)
}
