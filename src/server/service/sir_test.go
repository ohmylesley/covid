package service

import (
	"fmt"
)

func ExampleGetSir() {
	var initPop int = 10000
	var infect1 float64 = 2
	var recover1 float64 = 0.1
	initData := InitialData{
		N:             initPop,
		I1:            10,
		S1:            initPop,
		R1:            10,
		InfectionRate: infect1,
		RecoverRate:   recover1,
		Days:          30,
	}
	sirSlice := GetSir(initData)
	for i, v := range sirSlice {
		fmt.Println(i, v)
	}
	// Output: sir1
}
