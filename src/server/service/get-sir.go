package service

import (
	"math"
)

type SirSlice []SirData

type SirData struct {
	S int
	I int
	R int
}

type InitialData struct {
	N             int
	I1            int
	S1            int
	R1            int
	InfectionRate float64
	RecoverRate   float64
	Days          int
}

func toInt(x float64) int {
	val := int(math.Ceil(x))
	if val < 0 {
		val = 0
	}
	return val
}

func GetSir(x InitialData) SirSlice {
	var neg1 float64 = float64(-1)
	slice1 := make(SirSlice, x.Days)
	slice1[0] = SirData{x.S1, x.I1, x.R1}
	a := x.InfectionRate
	r := x.RecoverRate
	var pop float64 = float64(x.N)
	for i := 0; i < x.Days-1; i++ {
		obi := slice1[i]
		si := float64(obi.S)
		ii := float64(obi.I)
		ri := float64(obi.R)
		snext := toInt((neg1*a*si*ii)/pop + si)
		inext := toInt((a*si*ii)/pop + (neg1*r+1.0)*ii)
		rnext := toInt(r*ii + ri)
		slice1[i+1] = SirData{snext, inext, rnext}
	}
	return slice1
}
