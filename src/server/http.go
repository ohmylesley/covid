package main

import (
	"bytes"
	"encoding/json"
	"github.com/gorilla/mux"
	"gitlab.com/ohmylesley/covid/src/server/service"
	"log"
	"net/http"
)

func main() {
	log.Println("hello", service.Hello())
	router := mux.NewRouter().StrictSlash(true)
	router.HandleFunc("/calc", GetCalc).Methods(http.MethodGet, http.MethodPost, http.MethodOptions)
	router.Use(loggingMiddleware)
	router.Use(mux.CORSMethodMiddleware(router))
	log.Print(http.ListenAndServe(":4000", router))
}

func loggingMiddleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		var b bytes.Buffer
		b.ReadFrom(r.Body)
		log.Println(r.RequestURI, b.String())
		next.ServeHTTP(w, r)
	})
}

func GetCalc(w http.ResponseWriter, req *http.Request) {
	w.Header().Set("Access-Control-Allow-Origin", "*")
	if req.Method == http.MethodOptions {
		return
	}
	todos := Todos{
		Todo{"Jack", 12},
		Todo{"Pete", 22},
	}
	json.NewEncoder(w).Encode(todos)
}

type Todo struct {
	Name string `json:"name"`
	Age  int    `json:"myage"`
}

type Todos []Todo
